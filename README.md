# GEOPORTAL

Geoportal del semillero de investigación GEIPER.

## Run project, requires node js

### Clone or download this repo

```bash
  git clone https://gitlab.com/geiper/geoportal.git
```

### Install dependencies

```bash
  cd geoportal
  npm install
```


### Run the application locally

```bash
  npm start
```

Navigate to http://localhost:1234 to see the app running. The app will automatically reload if you change any of the source files. You can shut down the development server with a `Control C` any time you wish.
